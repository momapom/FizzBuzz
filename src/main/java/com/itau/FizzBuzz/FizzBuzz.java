package com.itau.FizzBuzz;

public class FizzBuzz {

    public String mostraSeq(int numero)
    {
       String sequencia = "";

       for (int i = 1; i <= numero; i++) {
   		 if(i % 3 == 0 && i % 5 == 0) {
			sequencia += "FizzBuzz ";
		 } 
   		 else { 
   		 if(i % 3 != 0 && i % 5 != 0) {
			sequencia += i + " ";
		 } 

   		 if(i % 3 == 0){
			sequencia += "Fizz ";
		 }

		 if(i % 5 == 0)
		 {
			sequencia += "Buzz ";
		 }
   		 }		
       }	
    System.out.println("sequencia ");
    System.out.println(sequencia);
	return sequencia.trim();
	}
}
