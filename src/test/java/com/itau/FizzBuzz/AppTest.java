package com.itau.FizzBuzz;

import org.junit.Test;

import junit.framework.TestCase;

public class AppTest 
    extends TestCase
{
//	@Test
//	public void testeFizzBuzzValidaSeq0() {
//		FizzBuzz fizzBuzz = new FizzBuzz();
//		String retorno = fizzBuzz.mostraSeq(0);
//		assertEquals(" ", retorno);	    
//	}
	
	@Test
	public void testeFizzBuzzValidaSeq1() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.mostraSeq(1);
		assertEquals("1", retorno);	    
	}
	
	@Test
	public void testeFizzBuzzValidaSeq3() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.mostraSeq(3);
		assertEquals("1 2 Fizz", retorno);	    
	}
	
	@Test
	public void testeFizzBuzzValidaSeq2() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.mostraSeq(2);
		assertEquals("1 2", retorno);	    
	}
	@Test
	public void testeFizzBuzzValidaSeq5() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.mostraSeq(5);
		assertEquals("1 2 Fizz 4 Buzz", retorno);	    
	}
	@Test
	public void testeFizzBuzzValidaSeq15() {
		FizzBuzz fizzBuzz = new FizzBuzz();
		String retorno = fizzBuzz.mostraSeq(15);
		assertEquals("1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz", retorno);	    
	}

}
